@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            Add Contacts
                        </div>
                         <div class="col-md-4">
                           
                        </div>
                        <div class="col-md-4">
                            <!-- <button type="button" class="btn btn-primary" href="{{route('add')}}">Add Contacts</button> -->

                        </div>
                         
                    </div>
                   
                </div>
                @if(session()->get('success'))
                  <div class="alert alert-success">
                    {{ session()->get('success') }}  
                  </div><br />
                @endif
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="post" action="{{route('update',$contact->id)}}" enctype="multipart/form-data">
                      @method('PATCH')
                      @csrf
                      
                          <div class="row">
                            <div class="col-md-4">
                            <label>First Name *</label>
                            <input type="text" class="form-control" name="first_name" value="{{$contact->first_name}}" required>
                          </div>
                          <div class="col-md-4">
                            <label>Middle Name </label>
                            <input type="text" class="form-control" name="middle_name"  value="{{ $contact->middle_name}}">
                          </div>
                          <div class="col-md-4">
                            <label>Last Name *</label>
                            <input type="text" class="form-control" name="last_name" required value="{{ $contact->last_name}}">
                          </div>
                          </div>
                          <div class="row">
                           <div class="col-md-4">
                            <label>Photo</label>
                            <input type="file" class="form-control btn btn-warning" name="photo" value="{{$contact->photo}}">
                          </div>
                          <div class="col-md-4">
                            <label>Photo</label>
                          <img src="{{$img}}" width="100" height="100">
                        </div>
                          <div class="col-md-4">

                            <label>Mobile Number</label>
                            <input type="number" class="form-control" name="mob_no" value="{{$contact->mob_no}}">
                          </div>
                         
                            
                          </div>
                          <div class="row">
                             <div class="col-md-4">
                            <label>Landline Number</label>
                            <input type="number" class="form-control" name="landline_no" value="{{$contact->landline_no}}">
                          </div>
                        
                        
                          <div class="col-md-8">
                            <label>Notes</label>
                            <textarea class="form-control" name="notes">{{$contact->notes}}</textarea>
                          </div>
                            
                          </div>
                          <input type="submit" class="btn btn-primary" name="submit" id="submit">
                      
                        </div>
                      
                     
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
  
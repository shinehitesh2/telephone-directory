@extends('layouts.app')

@section('content')
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.1.2/css/select.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/responsive/2.0.2/css/responsive.dataTables.min.css">


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            My Contacts
                        </div>
                         <div class="col-md-4">
                           
                        </div>
                        <div class="col-md-4">
                            <a href="{{route('add')}}"><button type="button" class="btn btn-primary" >Add Contacts</button></a>
                        </div>
                         
                    </div>
                   
                </div>


                <div class="card-body">
                    <form id="search-form" method="POST" action="{{route('searchbyname')}}">
                        @csrf
                        <input type="text" name="searchval" id="search"   value="">
                        <input type="submit" name="submit" value="Search" class="button btn-primary">
                      </form>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="example">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">First Name</th>
                          <th scope="col">Last Name</th>
                          <th scope="col">Mobile</th>
                          <th scope="col">Land Line</th>
                          <th scope="col">Date</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($contacts as $contact)
                        <tr>
                          
                          <th scope="row">{{$contact->id}}</th>
                          <td>{{$contact->first_name}}</td>
                          <td>{{$contact->last_name}}</td>
                          <td>{{$contact->mob_no}}</td>
                          <td>{{$contact->landline_no}}</td>
                          <td>{{date('d-m-Y', strtotime($contact->created_at))}}</td>
                          <td>
                            <a href="{{action('ContactController@view',$contact->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View" style="padding: 2px 6px 2px 6px;"><i class="fa fa-eye"></i></a>
                            <a href="{{action('ContactController@edit', $contact->id)}}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit" style="padding: 2px 6px 2px 6px;"><i class="fa fa-edit"></i></a>
                            <a href="{{action('ContactController@delete', $contact->id)}}" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" style="padding: 2px 6px 2px 6px;"><i class="fa fa-trash"></i></a>
                          </td>
                         
                        </tr>
                         @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="http://cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js"></script>
<script src="http://kingkode.com/datatables.editor.lite/js/altEditor/dataTables.altEditor.free.js"></script>
<script type="text/javascript">
  

   var myTable = $('#example').DataTable({
    "sPaginationType": "full_numbers",
    //data: dataSet,
    //columns: columnDefs,
    /*dom: 'Bfrtip',        // Needs button container
          select: 'single',
          responsive: true,
          altEditor: true,     // Enable altEditor
          buttons: [{
                  // do not change name
          }]*/
  });
   
function myFunction()
{
  var form = $('#search-form')[0];
  var data = new FormData(form);
  //ssvar searchValue = document.getElementById("search").value;
  //alert(searchValue);
  
    $.ajax({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'POST',           
                processData: false,  // Important!
                contentType: false,
                cache: false,  
                url:'{{route('search')}}',
                data:{data},

                success:function(data){
                    
                    $("#er_div").html(data.ExpectedRevenueTable).css('max-height','1020px');
                  
                    var table = $("#expected_revenue_table").DataTable({
                                                                             
                      "dom": 'Bfrtip',
                      "bPaginate": true,
                      "bAutoWidth": false,
                      "columnDefs": [{
                          "targets": 'no-sort',
                          "orderable": false
                      }]                                       //            "rowReorder": true,

                    });
                  }
                });
  }
</script>
@endsection

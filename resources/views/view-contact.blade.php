@extends('layouts.app')

@section('content')
<style type="text/css">
  canvas { background-color : #eee;
}
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            Add Contacts
                        </div>
                         <div class="col-md-4">
                           
                        </div>
                        <div class="col-md-4">
                            <!-- <button type="button" class="btn btn-primary" href="{{route('add')}}">Add Contacts</button> -->

                        </div>
                         
                    </div>
                   
                </div>
                @if(session()->get('success'))
                  <div class="alert alert-success">
                    {{ session()->get('success') }}  
                  </div><br />
                @endif
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  </div><br />
                @endif
                <div class="card-body">
                  <h6>Total Visits :{{$count}}</h6>
                  <br>
                   <h6>Weekly Visits : {{$weekCount}}</h6>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="post" action="{{route('store')}}" enctype="multipart/form-data">
                      @csrf
                      
                          <div class="row">
                            <div class="col-md-4">
                            <label>First Name *</label>
                            <input type="text" class="form-control" name="first_name" value="{{$contact->first_name}}" required readonly>
                          </div>
                          <div class="col-md-4">
                            <label>Middle Name </label>
                            <input type="text" class="form-control" name="middle_name"  value="{{ $contact->middle_name}}" readonly>
                          </div>
                          <div class="col-md-4">
                            <label>Last Name *</label>
                            <input type="text" class="form-control" name="last_name" required value="{{ $contact->last_name}}" readonly>
                          </div>
                          </div>
                          <div class="row">
                           
                          <div class="col-md-8">
                            <label>Photo</label>
                          <img src="{{$img}}" width="100" height="100">
                        </div>
                          <div class="col-md-4">

                            <label>Mobile Number</label>
                            <input type="number" class="form-control" name="mob_no" value="{{$contact->mob_no}}" readonly>
                          </div>
                         
                            
                          </div>
                          <div class="row">
                             <div class="col-md-4">
                            <label>Landline Number</label>
                            <input type="number" class="form-control" name="landline_no" value="{{$contact->landline_no}}" readonly>
                          </div>
                        
                        
                          <div class="col-md-8">
                            <label>Notes</label>
                            <textarea class="form-control" name="notes" readonly>{{$contact->notes}}</textarea>
                          </div>
                            
                          </div>
                          <!-- <input type="submit" class="btn btn-primary" name="submit" id="submit"> -->
                      
                        </div>
                      
                     
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-md-4">
    <canvas id="myChart"></canvas>
  </div>
  <div class="col-md-8">
    <!-- <canvas id="myChart1" width="200" height="100"></canvas> -->

  <!-- <canvas id="chartJSContainer" width="50" height="10"></canvas> -->
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <input type="hidden" name="" id="count" value="{{$count}}">
    
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <h1>{{--$increment--}}</h1>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <input type="hidden" name="" id="weekcount" value="{{$weekCount}}">
    <h1>{{--$weekCount--}}</h1>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.3/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>

<script type="text/javascript">
  var count = document.getElementById("count").value;
  var weekcount = document.getElementById("weekcount").value;
 


var ct = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ct, {
  type: 'pie',
  data: {
    labels: ["Daily Visits","weekly visits"],
    datasets: [{
      backgroundColor: [
        "#2ecc71",
        "#3498db"
      ],
      data: [count,weekcount]
    }]
  }
});


/*****************************************/

</script>


@endsection

  
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact/add', 'ContactController@add')->name('add');
Route::post('/contact/store', 'ContactController@store')->name('store');
Route::get('/contact/{id}/edit', 'ContactController@edit')->name('edit');
Route::get('/contact/{id}/view', 'ContactController@view')->name('view');
Route::patch('/contact/{id}/update', 'ContactController@update')->name('update');
Route::get('/contact/{id}/delete', 'ContactController@delete')->name('delete');
Route::post('/search', 'ContactController@search')->name('search');
Route::match(array('GET', 'POST'),'/searchbyname', 'ContactController@searchByName')->name('searchbyname');

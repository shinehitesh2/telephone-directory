<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
    	'first_name','middle_name','last_name','photo','mob_no','landline_no','user_id','visit_count','notes',
    ];
    public function visits()
	{
	    return visits($this);
	}
}

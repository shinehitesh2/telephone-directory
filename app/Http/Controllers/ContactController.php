<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Contact;
use Auth;
use DB;

class ContactController extends Controller
{	
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function add(){
        $contacts = Contact::all();
        
    	return view('add-contact',compact('contacts'));
    }
    public function store(Request $request){
        $userId = Auth::user()->id;
    	$request->validate([
        'first_name'=>'required',
        'middle_name'=>'',
        'last_name'=>'required',
        'photo' => 'image|max:500',
        'mob_no'=> 'integer|digits:10',
        'landline_no' => 'required|integer|digits:10',
        'notes' => ''
      ]);
    	$file = $request->file('photo');
        if ($file) {
            $profileImage = date('YmdHis') . "." . $file->getClientOriginalExtension();
            $destinationPath = public_path('/images');
           //dd($destinationPath);
            $file->move($destinationPath, $profileImage);

          $contact = new Contact([
            'first_name' => $request->get('first_name'),
            'middle_name'=> $request->get('middle_name'),
            'last_name'=> $request->get('last_name'),
            'photo'=> $profileImage,
            'mob_no'=> $request->get('mob_no'),
            'landline_no'=> $request->get('landline_no'),
            'notes'=> $request->get('notes'),
            'user_id'=> $userId,
          ]);
          $contact->save();
            # code...
        }else{
            $contact = new Contact([
            'first_name' => $request->get('first_name'),
            'middle_name'=> $request->get('middle_name'),
            'last_name'=> $request->get('last_name'),
            //'photo'=> $profileImage,
            'mob_no'=> $request->get('mob_no'),
            'landline_no'=> $request->get('landline_no'),
            'notes'=> $request->get('notes'),
            'user_id'=> $userId,
          ]);
          $contact->save();
        }
    	
      return redirect('/home')->with('success', 'Contact Added Successfully');
    	//return view('add-contact');
    }
    public function edit(Request $request,$id){
        $contact = Contact::find($id);
        $img = url('/images')."/".$contact->photo;
        //dd($img);
        return view('edit-contact',compact('contact','img'));
    }
    public function view(Request $request,$id){
      $contact = Contact::find($id);
      $count = $contact->visits()->count();
      $ipCount = $contact->visits()->increment();
      $weekCount = visits($contact)->period('week')->count();

        //$a = $contact->period('month')->top(10);
      // $a =  visits('App\Contact')->period('week');

        $increment =visits($contact)->forceIncrement();

       //$aa =  visits($contact)->increment();
      // dd($a);
        $img = url('/images')."/".$contact->photo;
        //dd($img);
        return view('view-contact',compact('contact','img','count','increment','weekCount','ipCount'));
    }
     public function update(Request $request,$id){
        $file = $request->file('photo');
        $contact = Contact::find($id);
        if ($file) {
            # code...
            $profileImage = date('YmdHis') . "." . $file->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $file->move($destinationPath, $profileImage);
            $contact->first_name=$request->get('first_name');
            $contact->middle_name=$request->get('middle_name');
            $contact->last_name=$request->get('last_name');
            $contact->photo=$profileImage;
            $contact->mob_no=$request->get('mob_no');
            $contact->landline_no=$request->get('landline_no');
            $contact->notes=$request->get('notes');
            $contact->save();
        }else{
            $contact->first_name=$request->get('first_name');
            $contact->middle_name=$request->get('middle_name');
            $contact->last_name=$request->get('last_name');
            //$contact->photo=$profileImage;
            $contact->mob_no=$request->get('mob_no');
            $contact->landline_no=$request->get('landline_no');
            $contact->notes=$request->get('notes');
            $contact->save();
        }
        

        
        
        return redirect('/home')->with('success', 'Contact Updated Successfully');
        
    }
    public function delete($id){
        Contact::find($id)->delete();
        return redirect('/home')->with('success', 'Contact Deleted Successfully');
    }
    /*public function search(Request $request){
        dd($request->all());
        $contacts = Contact::all();
        $html = '<table class="table" id="example">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">First Name</th>
                          <th scope="col">Last Name</th>
                          <th scope="col">Mobile</th>
                          <th scope="col">Land Line</th>
                          <th scope="col">Date</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>';
                        foreach($contacts as $contact){
                        $html .='<tr>
                          
                          <th scope="row">'.$contact->id.'</th>
                          <td>'.$contact->first_name.'</td>
                          <td>'.$contact->last_name.'</td>
                          <td>'.$contact->mob_no.'</td>
                          <td>'.$contact->landline_no.'</td>
                          <td>'.date('d-m-Y', strtotime($contact->created_at)).'</td>
                          <td>
                            <a href="" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="View" style="padding: 2px 6px 2px 6px;"><i class="fa fa-eye"></i></a>
                            <a href="" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit" style="padding: 2px 6px 2px 6px;"><i class="fa fa-edit"></i></a>
                            <a href="" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" style="padding: 2px 6px 2px 6px;"><i class="fa fa-trash"></i></a>
                          </td>
                         
                        </tr>';
                       }
                     $html .='</tbody>
                    </table>';
    }*/
    public function searchByName(Request $request){
        $searchVal = $request->get('searchval');
        $contacts=DB::table('contacts')->where('first_name','LIKE','%'.$searchVal."%")->orWhere('mob_no','LIKE','%'.$searchVal."%")->orWhere('landline_no','LIKE','%'.$searchVal."%")->get();
        return view('search-data',compact('contacts'));
    }

}
